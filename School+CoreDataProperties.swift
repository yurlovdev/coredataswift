//
//  School+CoreDataProperties.swift
//  CoreDataSwift
//
//  Created by James Roe on 24/09/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import Foundation
import CoreData

extension School {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<School> {
        return NSFetchRequest<School>(entityName: "School");
    }

    @NSManaged public var name: String?
    @NSManaged public var address: String?
    @NSManaged public var rating: String?
    @NSManaged public var type: String?
    @NSManaged public var students: NSSet?

}

// MARK: Generated accessors for students
extension School {

    @objc(addStudentsObject:)
    @NSManaged public func addToStudents(_ value: Students)

    @objc(removeStudentsObject:)
    @NSManaged public func removeFromStudents(_ value: Students)

    @objc(addStudents:)
    @NSManaged public func addToStudents(_ values: NSSet)

    @objc(removeStudents:)
    @NSManaged public func removeFromStudents(_ values: NSSet)

}
