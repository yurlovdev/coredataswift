//
//  Parents+CoreDataProperties.swift
//  CoreDataSwift
//
//  Created by James Roe on 24/09/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import Foundation
import CoreData

extension Parents {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Parents> {
        return NSFetchRequest<Parents>(entityName: "Parents");
    }

    @NSManaged public var name: String?
    @NSManaged public var age: Int16
    @NSManaged public var sex: String?
    @NSManaged public var students: NSSet?

}

// MARK: Generated accessors for students
extension Parents {

    @objc(addStudentsObject:)
    @NSManaged public func addToStudents(_ value: Students)

    @objc(removeStudentsObject:)
    @NSManaged public func removeFromStudents(_ value: Students)

    @objc(addStudents:)
    @NSManaged public func addToStudents(_ values: NSSet)

    @objc(removeStudents:)
    @NSManaged public func removeFromStudents(_ values: NSSet)

}
