//
//  StudentController.swift
//  CoreDataSwift
//
//  Created by James Roe on 24/09/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import UIKit

class StudentController: UITableViewController {
    
    var school : School? = nil
    var students : [Students] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = (self.school?.name)! + " students"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.students = self.school?.students?.allObjects as! [Students]
        tableView.reloadData()
    }

    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showParents" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! ParentController
                destinationController.student = self.students[indexPath.row]
            }
        }
        
        if segue.identifier == "addStudent" {
            let destinationController = segue.destination as! AddStudentController
            destinationController.school = self.school
        }
    }
    
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.students.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StudentTableViewCell

        let item = self.students[indexPath.row]
        
        cell.nameLabel.text = item.name
        cell.groupLabel.text = item.group
        cell.ageLabel.text = String(item.age)
        cell.gpaLabel.text = String(item.gpa)

        return cell
    }
    


    




}
