//
//  Students+CoreDataProperties.swift
//  CoreDataSwift
//
//  Created by James Roe on 24/09/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import Foundation
import CoreData

extension Students {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Students> {
        return NSFetchRequest<Students>(entityName: "Students");
    }

    @NSManaged public var name: String?
    @NSManaged public var group: String?
    @NSManaged public var gpa: Double
    @NSManaged public var age: Int16
    @NSManaged public var school: School?
    @NSManaged public var parent: NSSet?

}

// MARK: Generated accessors for parent
extension Students {

    @objc(addParentObject:)
    @NSManaged public func addToParent(_ value: Parents)

    @objc(removeParentObject:)
    @NSManaged public func removeFromParent(_ value: Parents)

    @objc(addParent:)
    @NSManaged public func addToParent(_ values: NSSet)

    @objc(removeParent:)
    @NSManaged public func removeFromParent(_ values: NSSet)

}
