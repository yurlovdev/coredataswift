//
//  ParentTableViewCell.swift
//  CoreDataSwift
//
//  Created by James Roe on 02/10/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import UIKit

class ParentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
