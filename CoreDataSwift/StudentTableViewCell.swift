//
//  StudentTableViewCell.swift
//  CoreDataSwift
//
//  Created by James Roe on 01/10/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var gpaLabel: UILabel!


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
