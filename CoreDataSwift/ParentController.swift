//
//  ParentController.swift
//  CoreDataSwift
//
//  Created by James Roe on 24/09/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import UIKit

class ParentController: UITableViewController {
    
    var student : Students? = nil
    var parents : [Parents] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        title = (self.student?.name)! + " parents"
    }

    override func viewWillAppear(_ animated: Bool) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            self.parents = try context.fetch(Parents.fetchRequest())
        }
        catch {
            print("ERROR")
        }
        tableView.reloadData()
    }

    
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.parents.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ParentTableViewCell
        
        let item = self.parents[indexPath.row]
        
        cell.nameLabel.text = item.name
        cell.ageLabel.text = String(item.age)
        cell.sexLabel.text = item.sex
        
        let parentArray : [Parents] = self.student?.parent?.allObjects as! [Parents]
        
        if parentArray.contains(item) {
            cell.accessoryType = .checkmark
        }
    
        //cell.accessoryType = .checkmark

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let parentArray : [Parents] = self.student?.parent?.allObjects as! [Parents]
        let item = self.parents[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath)
        
        if parentArray.contains(item) {
            self.student?.removeFromParent(self.parents[indexPath.row])
            cell?.accessoryType = .none
        }
        else {
            self.student?.addToParent(self.parents[indexPath.row])
            cell?.accessoryType = .checkmark
        }
        tableView.deselectRow(at: indexPath, animated: true)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*if segue.identifier == "addStudent" {
            let destinationController = segue.destination as! AddParentController
            destinationController.student = self.student
        }*/
    }


}
