//
//  AddSchoolController.swift
//  CoreDataSwift
//
//  Created by James Roe on 01/10/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import UIKit

class AddSchoolController: UIViewController {

    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var addressText: UITextField!
    @IBOutlet weak var typeText: UITextField!
    @IBOutlet weak var ratingText: UITextField!
    
    var school: School!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SavePress(_ sender: AnyObject) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let school = School(context: context)
        school.name = nameText.text
        school.address = addressText.text
        school.type = typeText.text
        school.rating = ratingText.text
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        navigationController!.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
