//
//  AddStudentController.swift
//  CoreDataSwift
//
//  Created by James Roe on 01/10/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import UIKit

class AddStudentController: UIViewController {
    
    var school : School? = nil
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var groupText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var gpaText: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func SavePress(_ sender: AnyObject) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let student = Students(context: context)
        student.name = self.nameText.text
        student.group = self.groupText.text
        student.age = Int16(self.ageText.text!)!
        student.gpa = Double(self.gpaText.text!)!
        student.school = self.school
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        navigationController!.popViewController(animated: true)
    }
}
