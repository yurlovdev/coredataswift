//
//  ViewController.swift
//  CoreDataSwift
//
//  Created by James Roe on 21/09/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var school : [School] = []
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            school = try context.fetch(School.fetchRequest())
        }
        catch {
            print("ERROR")
        }
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStudents" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! StudentController
                destinationController.school = school[indexPath.row]
            }
        }
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int  {
        return school.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SchoolTableViewCell
        let item = school[indexPath.row]
        
        cell.nameLabel.text = item.name
        cell.addressLabel.text = item.address
        cell.typeLabel.text = item.type
        cell.ratingLabel.text = item.rating
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        if editingStyle == .delete {
            let item = school[indexPath.row]
            context.delete(item)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
        do {
            school = try context.fetch(School.fetchRequest())
        }
        catch {
            print("ERROR")
        }
        tableView.reloadData()
    }
    
}

