//
//  AddParentController.swift
//  CoreDataSwift
//
//  Created by James Roe on 02/10/16.
//  Copyright © 2016 James Roe. All rights reserved.
//

import UIKit

class AddParentController: UIViewController {
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var sexText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SavePress(_ sender: AnyObject) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let parent = Parents(context: context)
        parent.name = self.nameText.text
        parent.sex = self.sexText.text
        parent.age = Int16(self.ageText.text!)!

        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        navigationController!.popViewController(animated: true)
    }

}
